﻿using Sale.Entities;
using Sale.Utils;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Sale.Data
{
    public class CompanyData
    {
        public int getNewCode()
        {
            try
            {
                List<Company> l = getAll();
                return (l.Count == 0 ? 0 : l.Max(p => p.Code)) + 1;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Company get(int Code)
        {
            try
            {
                return getAll("").Where(p => p.Code.Equals(Code)).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Company> getAll(string Name = "")
        {
            try
            {
                return JSONHelpers.ConverteJSonParaObject<List<Company>>(JSONHelpers.getJSonFileConten(FileHelpers.dbCompanyFile));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void add(Company p)
        {
            try
            {
                p.Code = getNewCode();
                List<Company> list = getAll() ?? new List<Company>();
                list.Add(p);
                JSONHelpers.setJSonToFile(list, FileHelpers.dbCompanyFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void delete(Company p)
        {
            try
            {
                List<Company> list = getAll().Where(x => !x.Code.Equals(p.Code)).ToList();
                JSONHelpers.setJSonToFile(list, FileHelpers.dbCompanyFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void update(Company p)
        {
            try
            {
                delete(p);
                add(p);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
