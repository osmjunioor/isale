﻿using Sale.Entities;
using Sale.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sale.Data
{
    public class PersonData
    {
        public int getNewCode()
        {
            try
            {
                return getAll().Max(p => p.Code) + 1;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Person get(int Code)
        {
            try
            {
                return getAll("").Where(p => p.Code.Equals(Code)).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Person> getAll(string Name = "")
        {
            try
            {
                return JSONHelpers.ConverteJSonParaObject<List<Person>>(JSONHelpers.getJSonFileConten(FileHelpers.dbPersonFile));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void add(Person p)
        {
            try
            {
                p.Code = getNewCode();
                List<Person> list = getAll() ?? new List<Person>();
                list.Add(p);
                JSONHelpers.setJSonToFile(list, FileHelpers.dbPersonFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void delete(Person p)
        {
            try
            {
                List<Person> list = getAll().Where(x => !x.Code.Equals(p.Code)).ToList();
                JSONHelpers.setJSonToFile(list, FileHelpers.dbPersonFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void update(Person p)
        {
            try
            {
                delete(p);
                add(p);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
