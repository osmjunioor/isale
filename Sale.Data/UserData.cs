﻿using Sale.Entities;
using Sale.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sale.Data
{
    public class UserData
    {
        public int getNewCode()
        {
            try
            {
                return getAll().Max(p => p.Code) + 1;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public User get(int Code)
        {
            try
            {
                return getAll().Where(p => p.Code.Equals(Code)).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public User get(string Login, string Password)
        {
            try
            {
                return getAll().Where(p => p.Login.Equals(Login) && p.Password.Equals(Password)).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<User> getAll(string Name = "")
        {
            try
            {
                return JSONHelpers.ConverteJSonParaObject<List<User>>(JSONHelpers.getJSonFileConten(FileHelpers.dbUserFile));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void add(User p)
        {
            try
            {
                p.Code = getNewCode();
                List<User> list = getAll() ?? new List<User>();
                list.Add(p);
                JSONHelpers.setJSonToFile(list, FileHelpers.dbUserFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void delete(User p)
        {
            try
            {
                List<User> list = getAll().Where(x => !x.Code.Equals(p.Code)).ToList();
                JSONHelpers.setJSonToFile(list, FileHelpers.dbUserFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void update(User p)
        {
            try
            {
                delete(p);
                add(p);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
