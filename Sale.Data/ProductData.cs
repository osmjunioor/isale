﻿using Sale.Entities;
using Sale.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sale.Data
{
    public class ProductData
    {
        public int getNewCode()
        {
            try
            {
                List<Product> l = getAll();
                return (l.Count == 0 ? 0 : l.Max(p => p.Code)) + 1;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Product get(int Code)
        {
            try
            {
                return getAll("").Where(p => p.Code.Equals(Code)).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Product> getAll(string Description = "")
        {
            try
            {
                return JSONHelpers.ConverteJSonParaObject<List<Product>>(JSONHelpers.getJSonFileConten(FileHelpers.dbProductFile));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void add(Product p)
        {
            try
            {
                p.Code = getNewCode();
                List<Product> list = getAll() ?? new List<Product>();
                list.Add(p);
                JSONHelpers.setJSonToFile(list, FileHelpers.dbProductFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void delete(Product p)
        {
            try
            {
                List<Product> list = getAll().Where(x => !x.Code.Equals(p.Code)).ToList();
                JSONHelpers.setJSonToFile(list, FileHelpers.dbProductFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void update(Product p)
        {
            try
            {
                delete(p);
                add(p);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
