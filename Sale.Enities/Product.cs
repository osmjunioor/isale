﻿using System.Runtime.Serialization;

namespace Sale.Entities
{
    [DataContract]
    public class Product
    {
        [DataMember]
        public int Code { get; set; }

        [DataMember]
        public string BarCode { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public decimal ValorVenda { get; set; }

        [DataMember]
        public decimal Estoque { get; set; }
    }
}
