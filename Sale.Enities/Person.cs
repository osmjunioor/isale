﻿using System.Runtime.Serialization;

namespace Sale.Entities
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public int Code { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
