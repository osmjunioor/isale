﻿using System.Runtime.Serialization;

namespace Sale.Entities
{
    public class User
    {
        [DataMember]
        public int Code { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
