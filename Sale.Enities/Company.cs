﻿using System.Runtime.Serialization;
namespace Sale.Entities
{
    [DataContract]
    public class Company
    {
        [DataMember]
        public int Code { get; set; }

        [DataMember]
        public string CpfCnpj { get; set; }

        [DataMember]
        public string SocialReason { get; set; }

        [DataMember]
        public string FantasyName { get; set; }
    }
}
