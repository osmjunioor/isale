﻿using System.IO;

namespace Sale.Utils
{
    public static class FileHelpers
    {
        public const string dbLocateDataBaseFolder = "Data";
        public const string dbPersonFile = "Data\\Person";
        public const string dbUserFile = "Data\\User";
        public const string dbProductFile = "Data\\Product";
        public const string dbCompanyFile = "Data\\Company";

        public static void createDataBase()
        {
            if (!Directory.Exists(dbLocateDataBaseFolder))
                Directory.CreateDirectory(dbLocateDataBaseFolder);
            if (!File.Exists(dbUserFile))
                File.Create(dbUserFile).Close();
            if (!File.Exists(dbPersonFile))
                File.Create(dbPersonFile).Close();
            if (!File.Exists(dbProductFile))
                File.Create(dbProductFile).Close();
            if (!File.Exists(dbCompanyFile))
                File.Create(dbCompanyFile).Close();
        }

    }
}
