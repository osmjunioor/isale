﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Sale.Utils
{
    public static class JSONHelpers
    {
        public static string ConverteObjectParaJSon<T>(T obj)
        {
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream();
                ser.WriteObject(ms, obj);
                string jsonString = Encoding.UTF8.GetString(ms.ToArray());
                ms.Close();
                return jsonString;
            }
            catch
            {
                throw;
            }
        }

        public static T ConverteJSonParaObject<T>(string jsonString)
        {
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                T obj = (T)serializer.ReadObject(ms);
                return obj;
            }
            catch
            {
                throw;
            }
        }

        public static string getJSonFileConten(string file)
        {
            try
            {
                string json = "";
                using (StreamReader r = new StreamReader(file))
                    json = r.ReadToEnd();
                return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool setJSonToFile<T>(T obj, string file)
        {
            try
            {
                string json = ConverteObjectParaJSon(obj);
                if (File.Exists(file + "_bkp"))
                    File.Delete(file + "_bkp");
                File.WriteAllText(file + "_bkp", json);
                if (File.Exists(file + "_old"))
                    File.Delete(file + "_old");
                File.Move(file, file + "_old");
                File.Move(file + "_bkp", file);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
