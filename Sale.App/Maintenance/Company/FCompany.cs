﻿using Sale.Data;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Sale.App.Maintenance.Company
{
    public partial class FCompany : Form
    {
        public FCompany()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Enable or disable fields
        /// </summary>
        /// <param name="Value">Enable value</param>
        private void enableOrDisable(bool Value)
        {
            txtCode.Enabled = !Value;
            txtNomeFantasia.Enabled = Value;
            txtRazaoSocial.Enabled = Value;
            txtCpfCnpj.Enabled = Value;
            btnAdd.Enabled = Value;
        }

        /// <summary>
        ///     Clears screen fields
        /// </summary>
        private void clearFields()
        {
            txtCode.Text = "";
            txtNomeFantasia.Text = "";
            txtRazaoSocial.Text = "";
            txtCpfCnpj.Text = "";
            btnAdd.Text = "Cadastrar";
        }

        /// <summary>
        ///     Check if fields are filled
        /// </summary>
        /// <returns>Verification result</returns>
        private bool fildIsValid()
        {
            label3.ForeColor = string.IsNullOrEmpty(txtNomeFantasia.Text) ? Color.Red : Color.Black;
            label4.ForeColor = string.IsNullOrEmpty(txtRazaoSocial.Text) ? Color.Red : Color.Black;
            label5.ForeColor = string.IsNullOrEmpty(txtCpfCnpj.Text) ? Color.Red : Color.Black;

            if (string.IsNullOrEmpty(txtNomeFantasia.Text) ||
                string.IsNullOrEmpty(txtRazaoSocial.Text) || string.IsNullOrEmpty(txtCpfCnpj.Text))
                return false;
            return true;
        }

        /// <summary>
        ///     Query and load company
        /// </summary>
        /// <param name="Code">Code company</param>
        private void getCompany(int Code)
        {
            Entities.Company c = new CompanyData().get(Code);
            if (c != null)
            {
                txtRazaoSocial.Text = c.SocialReason;
                txtCpfCnpj.Text = c.CpfCnpj;
                txtNomeFantasia.Text = c.FantasyName;
                enableOrDisable(true);
                btnAdd.Text = "Atualizar";
                txtNomeFantasia.Select();
            }
            else
                MessageBox.Show("Empresa não encontrada.");
        }

        /// <summary>
        ///     Add or change company
        /// </summary>
        private void insertOrUpdate()
        {
            if (fildIsValid())
            {
                Entities.Company c = new Entities.Company
                {
                    Code = string.IsNullOrEmpty(txtCode.Text) ? 0 : Convert.ToInt32(txtCode.Text),
                    CpfCnpj = txtCpfCnpj.Text.Trim(),
                    SocialReason = txtRazaoSocial.Text.Trim(),
                    FantasyName = txtNomeFantasia.Text.Trim()
                };

                if (c.Code == 0)
                {
                    new CompanyData().add(c);
                    txtCode.Text = c.Code.ToString();
                    MessageBox.Show("Empresa cadastrada com sucesso.");
                }
                else
                {
                    new CompanyData().update(c);
                    MessageBox.Show("Empresa atualizada com sucesso.");
                }
            }
        }

        /// <summary>
        ///     Delete company
        /// </summary>
        private void delete()
        {
            int code = string.IsNullOrEmpty(txtCode.Text) ? 0 : Convert.ToInt32(txtCode.Text);
            if (code > 0)
            {
                new CompanyData().delete(new Entities.Company() { Code = code });
                MessageBox.Show("Empresa excluida com sucesso.");
                btnCancel.PerformClick();
            }
        }

        private void txtCode_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        if (!string.IsNullOrEmpty(txtCode.Text))
                            getCompany(Convert.ToInt32(txtCode.Text));
                        else
                        {
                            enableOrDisable(true);
                            txtCpfCnpj.Select();
                        }
                        break;
                    }
                case Keys.Escape:
                    {
                        if (string.IsNullOrEmpty(txtCode.Text))
                            btnCancel.PerformClick();
                        break;
                    }
                case Keys.F10:
                    {
                        LCompany l = new LCompany();
                        l.Parent = this;
                        l.Size = Size;
                        l.Location = Location;
                        l.ShowDialog();
                        if (l.code > 0)
                            getCompany(l.code);
                        l.Dispose();
                        break;
                    }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            enableOrDisable(false);
            clearFields();
            txtCode.Select();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            insertOrUpdate();
        }

        private void FCompany_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    {
                        btnCancel.PerformClick();
                        break;
                    }
            }
        }

        private void FCompany_Load(object sender, EventArgs e)
        {
            btnCancel.PerformClick();
            txtCode.Text = "1";
            getCompany(1);
        }

        private void btnDelet_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja excluir? A operação não podera ser desfeita.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                delete();
        }
    }
}
