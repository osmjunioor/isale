﻿using Sale.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale.App.Maintenance.Company
{
    public partial class LCompany : Form
    {
        public int code = 0;

        public LCompany()
        {
            InitializeComponent();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            dgvData.DataSource = new CompanyData().getAll(txtSearch.Text.Trim());
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        dgvData.Select();
                        break;
                    }
                case Keys.Escape:
                    {
                        if (string.IsNullOrEmpty(txtSearch.Text))
                        {
                            code = 0;
                            Close();
                        }
                        break;
                    }
            }
        }

        private void dgvData_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        code = Convert.ToInt32(dgvData.CurrentRow.Cells[0].Value);
                        Close();
                        break;
                    }
                case Keys.Escape:
                    {
                        dgvData.DataSource = null;
                        txtSearch.Text = "";
                        txtSearch.Select();
                        break;
                    }
            }
        }
    }
}
