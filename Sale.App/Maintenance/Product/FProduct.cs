﻿using Sale.Data;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Sale.App.Maintenance.Product
{
    public partial class FProduct : Form
    {
        public FProduct()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Enable or disable fields
        /// </summary>
        /// <param name="Value">Enable value</param>
        private void enableOrDisable(bool Value)
        {
            txtCode.Enabled = !Value;
            txtBarCode.Enabled = Value;
            txtDescription.Enabled = Value;
            txtValorVenda.Enabled = Value;
            txtEstoque.Enabled = Value;
            btnAdd.Enabled = Value;
        }

        /// <summary>
        ///     Clears screen fields
        /// </summary>
        private void clearFields()
        {
            txtCode.Text = "";
            txtBarCode.Text = "";
            txtDescription.Text = "";
            txtValorVenda.Text = "";
            txtEstoque.Text = "";
            btnAdd.Text = "Cadastrar";
        }

        /// <summary>
        ///     Check if fields are filled
        /// </summary>
        /// <returns>Verification result</returns>
        private bool fildIsValid()
        {
            label2.ForeColor = string.IsNullOrEmpty(txtBarCode.Text) ? Color.Red : Color.Black;
            label3.ForeColor = string.IsNullOrEmpty(txtDescription.Text) ? Color.Red : Color.Black;
            label4.ForeColor = string.IsNullOrEmpty(txtValorVenda.Text) ? Color.Red : Color.Black;
            label5.ForeColor = string.IsNullOrEmpty(txtEstoque.Text) ? Color.Red : Color.Black;

            if (string.IsNullOrEmpty(txtBarCode.Text) || string.IsNullOrEmpty(txtDescription.Text) ||
                string.IsNullOrEmpty(txtValorVenda.Text) || string.IsNullOrEmpty(txtEstoque.Text))
                return false;
            return true;
        }

        /// <summary>
        ///     Query and load product
        /// </summary>
        /// <param name="Code">Code company</param>
        private void getProduto(int Codigo)
        {
            Entities.Product p = new ProductData().get(Codigo);
            if (p != null)
            {
                txtBarCode.Text = p.BarCode;
                txtDescription.Text = p.Description;
                txtEstoque.Text = p.Estoque.ToString("N2");
                txtValorVenda.Text = p.ValorVenda.ToString("N2");
                enableOrDisable(true);
                btnAdd.Text = "Atualizar";
                txtBarCode.Select();
            }
            else
                MessageBox.Show("Produto não encontrado.");
        }

        /// <summary>
        ///     Add or change company
        /// </summary>
        private void InsertOrUpdate()
        {
            if (fildIsValid())
            {
                Entities.Product p = new Entities.Product
                {
                    Code = string.IsNullOrEmpty(txtCode.Text) ? 0 : Convert.ToInt32(txtCode.Text),
                    BarCode = txtBarCode.Text.Trim(),
                    Description = txtDescription.Text.Trim(),
                    Estoque = Convert.ToDecimal(txtEstoque.Text),
                    ValorVenda = Convert.ToDecimal(txtValorVenda.Text)
                };

                if (p.Code == 0)
                {
                    new ProductData().add(p);
                    txtCode.Text = p.Code.ToString();
                    MessageBox.Show("Produto cadastrado com sucesso.");
                }
                else
                {
                    new ProductData().update(p);
                    MessageBox.Show("Produto atualizado com sucesso.");
                }
            }
        }

        /// <summary>
        ///     Delete company
        /// </summary>
        private void delete()
        {
            int code = string.IsNullOrEmpty(txtCode.Text) ? 0 : Convert.ToInt32(txtCode.Text);
            if (code > 0)
            {
                new ProductData().delete(new Entities.Product() { Code = code });
                MessageBox.Show("Produto excluido com sucesso.");
                btnCancel.PerformClick();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            InsertOrUpdate();
        }

        private void FProduct_Load(object sender, EventArgs e)
        {
            btnCancel.PerformClick();
        }

        private void txtCode_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        if (!string.IsNullOrEmpty(txtCode.Text))
                            getProduto(Convert.ToInt32(txtCode.Text));
                        else
                        {
                            enableOrDisable(true);
                            txtBarCode.Select();
                        }
                        break;
                    }
                case Keys.Escape:
                    {
                        if (string.IsNullOrEmpty(txtCode.Text))
                            btnCancel.PerformClick();
                        break;
                    }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            enableOrDisable(false);
            clearFields();
            txtCode.Select();
        }

        private void FProduct_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    {
                        btnCancel.PerformClick();
                        break;
                    }
            }
        }

        private void btnDelet_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja excluir? A operação não podera ser desfeita.", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                delete();
        }
    }
}
