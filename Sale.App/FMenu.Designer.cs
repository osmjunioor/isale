﻿namespace Sale.App
{
    partial class FMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMenu));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pForms = new System.Windows.Forms.Panel();
            this.lblBoasVindas = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pMaintenance = new System.Windows.Forms.Panel();
            this.btnMaintenanceUsers = new System.Windows.Forms.Button();
            this.btnMaintenanceProducts = new System.Windows.Forms.Button();
            this.btnMaintenanceCompany = new System.Windows.Forms.Button();
            this.btnMaintenance = new System.Windows.Forms.Button();
            this.iListButtonIcon = new System.Windows.Forms.ImageList(this.components);
            this.btnHome = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUsuario = new System.Windows.Forms.Button();
            this.lblCompany = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.pForms.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pMaintenance.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.pForms);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panel1.Size = new System.Drawing.Size(796, 594);
            this.panel1.TabIndex = 0;
            // 
            // pForms
            // 
            this.pForms.BackColor = System.Drawing.Color.White;
            this.pForms.Controls.Add(this.lblBoasVindas);
            this.pForms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pForms.Location = new System.Drawing.Point(151, 78);
            this.pForms.Name = "pForms";
            this.pForms.Size = new System.Drawing.Size(635, 516);
            this.pForms.TabIndex = 23;
            // 
            // lblBoasVindas
            // 
            this.lblBoasVindas.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBoasVindas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBoasVindas.Font = new System.Drawing.Font("Dubai", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoasVindas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.lblBoasVindas.Location = new System.Drawing.Point(0, 0);
            this.lblBoasVindas.Name = "lblBoasVindas";
            this.lblBoasVindas.Size = new System.Drawing.Size(635, 52);
            this.lblBoasVindas.TabIndex = 23;
            this.lblBoasVindas.Text = "Olá , bem vindo(a) ao iSale!";
            this.lblBoasVindas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(151, 68);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(635, 10);
            this.panel2.TabIndex = 24;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.pMaintenance);
            this.panel3.Controls.Add(this.btnMaintenance);
            this.panel3.Controls.Add(this.btnHome);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(10, 68);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.panel3.Size = new System.Drawing.Size(141, 526);
            this.panel3.TabIndex = 22;
            // 
            // pMaintenance
            // 
            this.pMaintenance.AutoSize = true;
            this.pMaintenance.Controls.Add(this.btnMaintenanceUsers);
            this.pMaintenance.Controls.Add(this.btnMaintenanceProducts);
            this.pMaintenance.Controls.Add(this.btnMaintenanceCompany);
            this.pMaintenance.Dock = System.Windows.Forms.DockStyle.Top;
            this.pMaintenance.Location = new System.Drawing.Point(0, 84);
            this.pMaintenance.Name = "pMaintenance";
            this.pMaintenance.Size = new System.Drawing.Size(141, 93);
            this.pMaintenance.TabIndex = 21;
            this.pMaintenance.Visible = false;
            // 
            // btnMaintenanceUsers
            // 
            this.btnMaintenanceUsers.AutoSize = true;
            this.btnMaintenanceUsers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaintenanceUsers.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMaintenanceUsers.FlatAppearance.BorderSize = 0;
            this.btnMaintenanceUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaintenanceUsers.Font = new System.Drawing.Font("Dubai", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaintenanceUsers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(132)))), ((int)(((byte)(151)))));
            this.btnMaintenanceUsers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenanceUsers.ImageKey = "home.png";
            this.btnMaintenanceUsers.Location = new System.Drawing.Point(0, 62);
            this.btnMaintenanceUsers.Name = "btnMaintenanceUsers";
            this.btnMaintenanceUsers.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnMaintenanceUsers.Size = new System.Drawing.Size(141, 31);
            this.btnMaintenanceUsers.TabIndex = 27;
            this.btnMaintenanceUsers.Text = "Usuários";
            this.btnMaintenanceUsers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenanceUsers.UseVisualStyleBackColor = true;
            // 
            // btnMaintenanceProducts
            // 
            this.btnMaintenanceProducts.AutoSize = true;
            this.btnMaintenanceProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaintenanceProducts.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMaintenanceProducts.FlatAppearance.BorderSize = 0;
            this.btnMaintenanceProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaintenanceProducts.Font = new System.Drawing.Font("Dubai", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaintenanceProducts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(132)))), ((int)(((byte)(151)))));
            this.btnMaintenanceProducts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenanceProducts.ImageKey = "home.png";
            this.btnMaintenanceProducts.Location = new System.Drawing.Point(0, 31);
            this.btnMaintenanceProducts.Name = "btnMaintenanceProducts";
            this.btnMaintenanceProducts.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnMaintenanceProducts.Size = new System.Drawing.Size(141, 31);
            this.btnMaintenanceProducts.TabIndex = 25;
            this.btnMaintenanceProducts.Text = "Produtos";
            this.btnMaintenanceProducts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenanceProducts.UseVisualStyleBackColor = true;
            this.btnMaintenanceProducts.Click += new System.EventHandler(this.btnMaintenanceProducts_Click);
            // 
            // btnMaintenanceCompany
            // 
            this.btnMaintenanceCompany.AutoSize = true;
            this.btnMaintenanceCompany.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaintenanceCompany.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMaintenanceCompany.FlatAppearance.BorderSize = 0;
            this.btnMaintenanceCompany.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaintenanceCompany.Font = new System.Drawing.Font("Dubai", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaintenanceCompany.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(132)))), ((int)(((byte)(151)))));
            this.btnMaintenanceCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenanceCompany.ImageKey = "home.png";
            this.btnMaintenanceCompany.Location = new System.Drawing.Point(0, 0);
            this.btnMaintenanceCompany.Name = "btnMaintenanceCompany";
            this.btnMaintenanceCompany.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnMaintenanceCompany.Size = new System.Drawing.Size(141, 31);
            this.btnMaintenanceCompany.TabIndex = 26;
            this.btnMaintenanceCompany.Text = "Empresa";
            this.btnMaintenanceCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenanceCompany.UseVisualStyleBackColor = true;
            this.btnMaintenanceCompany.Click += new System.EventHandler(this.btnMaintenanceCompany_Click);
            // 
            // btnMaintenance
            // 
            this.btnMaintenance.AutoSize = true;
            this.btnMaintenance.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaintenance.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMaintenance.FlatAppearance.BorderSize = 0;
            this.btnMaintenance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaintenance.Font = new System.Drawing.Font("Dubai", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaintenance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.btnMaintenance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaintenance.ImageKey = "cadastro.png";
            this.btnMaintenance.ImageList = this.iListButtonIcon;
            this.btnMaintenance.Location = new System.Drawing.Point(0, 52);
            this.btnMaintenance.Name = "btnMaintenance";
            this.btnMaintenance.Size = new System.Drawing.Size(141, 32);
            this.btnMaintenance.TabIndex = 20;
            this.btnMaintenance.Text = "   CADASTROS";
            this.btnMaintenance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMaintenance.UseVisualStyleBackColor = true;
            this.btnMaintenance.Click += new System.EventHandler(this.btnMaintenance_Click);
            // 
            // iListButtonIcon
            // 
            this.iListButtonIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iListButtonIcon.ImageStream")));
            this.iListButtonIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.iListButtonIcon.Images.SetKeyName(0, "if_menu-alt_134216.png");
            this.iListButtonIcon.Images.SetKeyName(1, "home.png");
            this.iListButtonIcon.Images.SetKeyName(2, "cadastro.png");
            // 
            // btnHome
            // 
            this.btnHome.AutoSize = true;
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Dubai", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.ImageKey = "home.png";
            this.btnHome.ImageList = this.iListButtonIcon;
            this.btnHome.Location = new System.Drawing.Point(0, 20);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(141, 32);
            this.btnHome.TabIndex = 22;
            this.btnHome.Text = "   VISÃO GERAL";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.btnUsuario);
            this.panel4.Controls.Add(this.lblCompany);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(10, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(776, 58);
            this.panel4.TabIndex = 18;
            // 
            // btnUsuario
            // 
            this.btnUsuario.AutoSize = true;
            this.btnUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUsuario.FlatAppearance.BorderSize = 0;
            this.btnUsuario.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnUsuario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsuario.Font = new System.Drawing.Font("Dubai", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(132)))), ((int)(((byte)(151)))));
            this.btnUsuario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUsuario.ImageKey = "home.png";
            this.btnUsuario.Location = new System.Drawing.Point(485, 23);
            this.btnUsuario.Name = "btnUsuario";
            this.btnUsuario.Size = new System.Drawing.Size(287, 31);
            this.btnUsuario.TabIndex = 21;
            this.btnUsuario.Text = "Usuário Logado";
            this.btnUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUsuario.UseVisualStyleBackColor = true;
            this.btnUsuario.Click += new System.EventHandler(this.btnUsuario_Click);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCompany.Font = new System.Drawing.Font("Dubai", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.lblCompany.Location = new System.Drawing.Point(144, 24);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(96, 27);
            this.lblCompany.TabIndex = 11;
            this.lblCompany.Text = "Sua Empresa";
            // 
            // label3
            // 
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Dubai Medium", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.label3.Location = new System.Drawing.Point(50, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 58);
            this.label3.TabIndex = 9;
            this.label3.Text = "iSale";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Dubai", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "FMenu";
            this.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iSale";
            this.Load += new System.EventHandler(this.FMenu_Load);
            this.panel1.ResumeLayout(false);
            this.pForms.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pMaintenance.ResumeLayout(false);
            this.pMaintenance.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMaintenance;
        private System.Windows.Forms.ImageList iListButtonIcon;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnUsuario;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pMaintenance;
        private System.Windows.Forms.Button btnMaintenanceUsers;
        private System.Windows.Forms.Button btnMaintenanceProducts;
        private System.Windows.Forms.Button btnMaintenanceCompany;
        private System.Windows.Forms.Panel pForms;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblBoasVindas;
    }
}