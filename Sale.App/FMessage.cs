﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale.App
{
    public partial class FMessage : Form
    {
        public enum ButtonType
        {
            YesOrNo,
            OkOrCancel
        }
        public FMessage()
        {
            InitializeComponent();
        }

        private void FMessage_Load(object sender, EventArgs e)
        {

        }

        public void Show(string Text, string Caption, ButtonType Button)
        {
            lblMessage.Text = Text;
            lblTitle.Text = Caption;

            switch (Button)
            {
                case ButtonType.YesOrNo:
                    {
                        btnSim.Visible = true;
                        btnNao.Visible = true;
                        btnCancelar.Visible = false;
                        btnSim.Text = "Sim";
                        btnNao.Text = "Não";
                    }
                    break;
                case ButtonType.OkOrCancel:
                    {
                        btnSim.Visible = true;
                        btnNao.Visible = false;
                        btnCancelar.Visible = true;
                        btnSim.Text = "Ok";
                        btnCancelar.Text = "Cancelar";
                    }
                    break;
                default:
                    break;
            }
            Show();
        }
    }
}
