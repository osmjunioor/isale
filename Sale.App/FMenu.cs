﻿using Sale.Data;
using Sale.Entities;
using System;
using System.Windows.Forms;

namespace Sale.App
{
    public partial class FMenu : Form
    {
        public FMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Load default company data
        /// </summary>
        private void getDefaultCompany()
        {
            Company company = new CompanyData().get(1);
            lblCompany.Text = company == null ? "Empresa não cadastrada" : company.FantasyName;
        }

        /// <summary>
        ///     Show form within panel
        /// </summary>
        /// <param name="f">Form</param>
        private void ShowForm(Form f)
        {
            pForms.Controls.Clear();
            f.TopLevel = false;
            f.AutoScroll = true;
            pForms.Controls.Add(f);
            f.Size = pForms.Size;
            lblBoasVindas.Visible = false;
            f.Show();
        }

        private void FMenu_Load(object sender, EventArgs e)
        {
            btnUsuario.Text = FLogin.personLocate.Name + " - Sair";
            lblBoasVindas.Text = "Olá " + FLogin.personLocate.Name + ", bem vindo(a) ao iSale! :)";
            getDefaultCompany();
        }

        private void btnMaintenance_Click(object sender, EventArgs e)
        {
            pMaintenance.Visible = !pMaintenance.Visible;
        }

        private void btnMaintenanceProducts_Click(object sender, EventArgs e)
        {
            ShowForm(new Maintenance.Product.FProduct());
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja encerrar o sistema?", "Até logo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }

        private void btnMaintenanceCompany_Click(object sender, EventArgs e)
        {
            ShowForm(new Maintenance.Company.FCompany());
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            getDefaultCompany();
            pForms.Controls.Clear();
            lblBoasVindas.Visible = true;
            pForms.Controls.Add(lblBoasVindas);
        }
    }
}
