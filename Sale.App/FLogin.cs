﻿using Sale.Data;
using Sale.Entities;
using Sale.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Sale.App
{
    public partial class FLogin : Form
    {
        public FLogin()
        {
            InitializeComponent();
        }

        public static Person personLocate = null;

        public static User userLocate = null;

        private void FLogin_Load(object sender, EventArgs e)
        {
            FileHelpers.createDataBase();

            if (false)
            {
                var u = new List<User>
                {
                    new User { Code = 1, Login = "jose", Password = "123" },
                    new User { Code = 2, Login = "teste", Password = "123" }
                };

                var p = new List<Person>
                {
                    new Person { Code = 1, Name = "José Maria" },
                    new Person { Code = 2, Name = "Usuário de Testes" }
                };

                JSONHelpers.setJSonToFile(u, FileHelpers.dbUserFile);
                JSONHelpers.setJSonToFile(p, FileHelpers.dbPersonFile);
            }


        }

        private bool getLogin(string login, string pass)
        {
            userLocate = new UserData().get(login, pass);
            if (userLocate == null)
                return false;
            personLocate = new PersonData().get(userLocate.Code);
            if (personLocate == null)
                return false;
            return true;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (getLogin(txtLogin.Text, txtPassword.Text))
            {
                Hide();
                Refresh();
                FMenu f = new FMenu();
                f.ShowDialog();
                this.Close();
            }
            else
                lblMsg.Text = "Usuário não encontrado";
        }

        private void label1_TextChanged(object sender, EventArgs e)
        {
            lblMsg.Visible = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lblMsg.Text))
                lblMsg.Text = "";
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lblMsg.Text))
                lblMsg.Text = "";
        }

        private void lblDeveloper_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Enviar email ao desenvolvedor?", "Enviar email para osmar.dev@outlook.com", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Process.Start("mailto:osmar.dev@outlook.com");
        }
    }
}
